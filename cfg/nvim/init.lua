-- {{{VIM.OPT   {{{
vim.g.mapleader = " "				
vim.g.netrw_banner = 0		
vim.g.netrw_browse_split=4
vim.g.netrw_altv = 1		
vim.g.netrw_liststyle=3		
vim.opt.title = true					
vim.cmd('set path+=**')					
vim.opt.syntax = "ON"
vim.opt.backup = false
vim.opt.compatible = false				
vim.opt.number = true					
vim.opt.relativenumber = true				
vim.opt.mouse = 'a'						
vim.opt.ignorecase = true				
vim.opt.smartcase = true				
vim.opt.hlsearch = false				
vim.opt.incsearch = true				
vim.opt.wrap = false
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.fileencoding = "utf-8"
vim.opt.pumheight = 10					
vim.opt.showtabline = 2					
vim.opt.laststatus = 2					
vim.opt.signcolumn = "auto"
vim.opt.expandtab = false				
vim.opt.smartindent = true
vim.opt.showcmd = true
vim.opt.cmdheight = 2
vim.opt.showmode = true
vim.opt.scrolloff = 8					
vim.opt.sidescrolloff = 8		
vim.opt.clipboard = unnamedplus
vim.opt.splitbelow = true				
vim.opt.splitright = true
vim.opt.cursorline = true
vim.opt.swapfile = false
vim.opt.scrolloff = 10
vim.opt.timeoutlen = 100
vim.opt.updatetime = 16
vim.opt.guifont = "Operator Mono Lig"
-- }}}
-- {{{   
-- Space as leader key {{{
vim.g.mapleader = ' '
-- }}}
-- Commands {{{
vim.keymap.set('n', '<leader>bq', '<cmd>bdelete<cr>')
vim.keymap.set('n', '<leader>q', '<cmd>quit<cr>')
vim.keymap.set('n', '<leader>v', ':vsplit')
vim.keymap.set('n', '<leader>h', ':split')
vim.keymap.set('n', '<leader>w', '<cmd>write<cr>')
vim.keymap.set('n', '<leader>c', 'zc')
vim.keymap.set("n", "<C-h>", "<C-w>h")						-- control+h switches to left split
vim.keymap.set("n", "<C-l>", "<C-w>l")						-- control+l switches to right split
vim.keymap.set("n", "<C-j>", "<C-w>j")						-- control+j switches to bottom split
vim.keymap.set("n", "<C-k>", "<C-w>k")	
vim.keymap.set("n", "<C-Left>", ":vertical resize +3<CR>")
vim.keymap.set("n", "<C-Right>", ":vertical resize -3<CR>")
vim.keymap.set("n", "<C-Up>", ":resize +3<CR>")
vim.keymap.set("n", "<C-Down>", ":resize -3<CR>")
vim.keymap.set("i", "kj", "<Esc>")
vim.keymap.set("i", "jk", "<Esc>")
vim.keymap.set("i", "'", "''<left>")
vim.keymap.set("i", "\"", "\"\"<left>")
vim.keymap.set("i", "(", "()<left>")
vim.keymap.set("i", "[", "[]<left>")
vim.keymap.set("i", "{", "{}<left>")
vim.keymap.set("i", "{;", "{};<left><left>")
vim.keymap.set("i", "/*", "/**/<left><left>")

vim.opt.guicursor             = "v-c-sm:block,n-i-ci-ve:ver25,r-cr-o:hor20,i-ci:hor30-iCursor,c-ci-cr:hor20-Cursor/lCursor"
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv") -- Allow me to move lines easly with <J> and <K>
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
vim.keymap.set("n", "<leader>e", ":25Lex<CR>")
-- }}}

-- {{{
local lazy = {}
function lazy.install(path)
  if not vim.loop.fs_stat(path) then
	print('Installing lazy.nvim....')
	vim.fn.system({
	  'git',
	  'clone',
	  '--filter=blob:none',
	  'https://github.com/folke/lazy.nvim.git',
	  '--branch=stable', -- latest stable release
	  path,
	})
  end
end
function lazy.setup(plugins)
  if vim.g.plugins_ready then
	return
  end
  vim.opt.rtp:prepend(lazy.path)
  require('lazy').setup(plugins, lazy.opts)
  vim.g.plugins_ready = true
end
lazy.path = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
lazy.opts = {}
lazy.setup({
  {'folke/tokyonight.nvim'},
  {"thePrimeagen/vim-be-good"},
  {'Mofiqul/dracula.nvim'},
  {'kyazdani42/nvim-web-devicons'},
  {'nvim-lualine/lualine.nvim'},
})

vim.opt.showmode = false
-- require("VimBeGood").setup {}
require('lualine').setup({
  options = {
	icons_enabled = false,
	theme = 'dracula',
	component_separators = '|',
	section_separators = '',
  },
})
-- customize dracula color palette
require('dracula').setup({
  colors = {
    bg = "#282A36",
    fg = "#F8F8F2",
    selection = "#44475A",
    comment = "#6272A4",
    red = "#FF5555",
    orange = "#FFB86C",
    yellow = "#F1FA8C",
    green = "#50fa7b",
    purple = "#BD93F9",
    cyan = "#8BE9FD",
    pink = "#FF79C6",
    bright_red = "#FF6E6E",
    bright_green = "#69FF94",
    bright_yellow = "#FFFFA5",
    bright_blue = "#D6ACFF",
    bright_magenta = "#FF92DF",
    bright_cyan = "#A4FFFF",
    bright_white = "#FFFFFF",
    menu = "#21222C",
    visual = "#3E4452",
    gutter_fg = "#4B5263",
    nontext = "#3B4048",
    white = "#ABB2BF",
    black = "#191A21",
  },
  -- show the '~' characters after the end of buffers
  show_end_of_buffer = true, -- default false
  -- use transparent background
  transparent_bg = true, -- default false
  -- set custom lualine background color
  lualine_bg_color = "#44475a", -- default nil
  -- set italic comment
  italic_comment = true, -- default false
  -- overrides the default highlights with table see `:h synIDattr`
  overrides = {},
})
require("tokyonight").setup({
  -- your configuration comes here
  -- or leave it empty to use the default settings
  style = "storm", -- The theme comes in three styles, `storm`, `moon`, a darker variant `night` and `day`
  light_style = "day", -- The theme is used when the background is set to light
  transparent = true, -- Enable this to disable setting the background color
  terminal_colors = true, -- Configure the colors used when opening a `:terminal` in [Neovim](https://github.com/neovim/neovim)
  styles = {
    -- Style to be applied to different syntax groups
    -- Value is any valid attr-list value for `:help nvim_set_hl`
    comments = { italic = true },
    keywords = { italic = true },
    functions = {},
    variables = {},
    -- Background styles. Can be "dark", "transparent" or "normal"
    sidebars = "dark", -- style for sidebars, see below
    floats = "dark", -- style for floating windows
  },
  sidebars = { "qf", "help" }, -- Set a darker background on sidebar-like windows. For example: `["qf", "vista_kind", "terminal", "packer"]`
  day_brightness = 0.3, -- Adjusts the brightness of the colors of the **Day** style. Number between 0 and 1, from dull to vibrant colors
  hide_inactive_statusline = false, -- Enabling this option, will hide inactive statuslines and replace them with a thin border instead. Should work with the standard **StatusLine** and **LuaLine**.
  dim_inactive = false, -- dims inactive windows
  lualine_bold = false, -- When `true`, section headers in the lualine theme will be bold

  --- You can override specific color groups to use other groups or a hex color
  --- function will be called with a ColorScheme table
  ---@param colors ColorScheme
  on_colors = function(colors) end,

  --- You can override specific highlights to use other groups or a hex color
  --- function will be called with a Highlights and ColorScheme table
  ---@param highlights Highlights
  ---@param colors ColorScheme
  on_highlights = function(highlights, colors) end,
})

vim.opt.termguicolors = true
vim.cmd.colorscheme('tokyonight')
-- }}}
-- }}}}}}
-- vim:foldmethod=marker:foldlevel=0

